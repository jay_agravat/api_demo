import 'dart:convert';

import 'package:api_demo/insert_user.dart';
import 'package:api_demo/second_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:http/http.dart' as http;

class FirstPage extends StatefulWidget {
  FirstPage({Key? key}) : super(key: key);

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  List<Map> users = [];
  List<Map> restaurants = [];

  void initUsers() {
    Map<String, dynamic> map = {};
    map['Name'] = 'Fast Food';
    map['img'] = 'assets/images/fast_food.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Fruites';
    map['img'] = 'assets/images/frueit.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Pizza';
    map['img'] = 'assets/images/Pizza.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Sweet';
    map['img'] = 'assets/images/sweet.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Cold Drinks';
    map['img'] = 'assets/images/Cold_drinks.jpeg';
    users.add(map);
  }

  void initRestaurants() {
    Map<String, dynamic> restaurant = {};
    restaurant['Name'] = 'Foodcave Restaurant';
    restaurant['img'] = 'assets/images/restaurants_1.jpeg';
    restaurant['location'] = 'New York, Australia';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Momai Restaurant';
    restaurant['img'] = 'assets/images/restaurants_2.jpeg';
    restaurant['location'] = 'Rajkot, India';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Cheesy Restaurant';
    restaurant['img'] = 'assets/images/restaurants_3.jpeg';
    restaurant['location'] = 'california, US';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Cafe Yum Restaurant';
    restaurant['img'] = 'assets/images/restaurants_4.jpeg';
    restaurant['location'] = 'New York, Australia';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Thai Tanic Restaurant';
    restaurant['img'] = 'assets/images/restaurants_5.jpeg';
    restaurant['location'] = 'hong kong ,disneyland';
    restaurants.add(restaurant);
  }

  @override
  void initState() {
    initUsers();
    initRestaurants();
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Api Demo"),
        centerTitle: true,
        leading: Container(
          width: 30,
          height: 30,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Icon(Icons.menu),
        ),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return Insertuser(
                      map: null,
                    );
                  },
                ),
              ).then(
                (value) {
                  setState(() {});
                },
              );
            },
            child: Icon(Icons.add),
          ),
          Container(
            margin: EdgeInsets.only(left: 30, right: 2),
            width: 50,
            height: 20,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(60),
                child: Image.asset('assets/images/first_page_top_photo.png')),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                height: 45,
                margin: EdgeInsets.all(20),
                // width: 700,
                color: Colors.white70,
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Container(
                        // width: 45,
                        // color: Colors.black12,
                        child: TextField(
                          style: TextStyle(
                            color: Colors.black87,
                            // backgroundColor: Colors.white70,
                          ),
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.black26,
                            floatingLabelAlignment:
                                FloatingLabelAlignment.center,
                            prefixIcon: Icon(Icons.search),
                            hintText: 'Healty Food',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: SizedBox(
                        width: 20,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 80,
                        // width: 10,
                        // color: Colors.red,
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                        child: Icon(Icons.tune_outlined),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                // margin: EdgeInsets.only(top: 20),
                height: 45,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(right: 10),
                      padding: EdgeInsets.only(left: 10),
                      width: 140,
                      height: 30,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                            radius: 15,
                            backgroundImage: AssetImage(
                              users[index]['img'].toString(),
                              // 'assets/images/first_page_top_photo.png',
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            users[index]['Name'],
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: users.length,
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Container(
                height: 220,
                child: FutureBuilder<http.Response>(
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      dynamic jsonData =
                          jsonDecode(snapshot.data!.body.toString());
                      return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: jsonData.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return SecondPage(
                                      foodImage: jsonData[index]["img"],
                                      foodName: jsonData[index]["Name"],
                                      foodPrice: jsonData[index]["price"],
                                      isFav: jsonData[index]["isfav"],
                                    );
                                  },
                                ),
                              );
                            },
                            child: Container(
                              width: 120,
                              margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 8,
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  Stack(
                                    children: [
                                      Container(
                                        // width: double.infinity,
                                        // height: 100,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          child: Image.network(
                                            height: 80,
                                            jsonData[index]["img"].toString(),
                                            // fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: 5,
                                        right: 5,
                                        child: Container(
                                          width: 30,
                                          height: 30,
                                          decoration: BoxDecoration(
                                            color: Colors.grey,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: InkWell(
                                            onTap: () {
                                              deleteData(jsonData[index]["id"])
                                                  .then(
                                                (value) {
                                                  setState(() {});
                                                },
                                              );
                                            },
                                            child: Icon(Icons.delete),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    jsonData[index]["Name"].toString(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(jsonData[index]["test"].toString()),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                          "\$${jsonData[index]["price"].toString()}"),
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            jsonData[index]["isfav"] =
                                                !jsonData[index]["isfav"];
                                          });
                                        },
                                        child: Icon(
                                          jsonData[index]["isfav"]
                                              ? Icons.favorite
                                              : Icons.favorite_border,
                                          color: Colors.red,
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) {
                                                return Insertuser(
                                                  map: jsonData[index],
                                                );
                                              },
                                            ),
                                          ).then(
                                            (value) {
                                              setState(() {});
                                            },
                                          );
                                        },
                                        child: Icon(Icons.edit),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  },
                  future: getData(),
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Favorite Restaurants',
                    style: TextStyle(fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'See all',
                    style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Container(
                height: 130,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      width: 267,
                      margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 8,
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Container(
                                // width: double.infinity,
                                // height: 100,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage(
                                    restaurants[index]['img'].toString(),
                                  ),
                                  radius: 40,
                                ),
                                margin: EdgeInsets.all(6),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    restaurants[index]['Name'],
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    restaurants[index]['location'],
                                    style: TextStyle(
                                        // fontWeight: FontWeight.w700,
                                        ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  RatingBar.builder(
                                    itemSize: 20,
                                    glowColor: Colors.red,
                                    initialRating: 3,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemPadding: EdgeInsets.only(right: 5.0),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: restaurants.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<http.Response> getData() async {
    http.Response res = await http
        .get(Uri.parse('https://63f8de8b5b0e4a127df030ce.mockapi.io/Items'));
    print(res.body);

    return res;
  }

  Future<void> deleteData(id) async {
    http.Response res = await http.delete(
        Uri.parse('https://63f8de8b5b0e4a127df030ce.mockapi.io/Items/$id'));
  }
}
