import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Insertuser extends StatefulWidget {
  Insertuser({super.key, this.map});

  Map? map;

  @override
  State<Insertuser> createState() => _InsertuserState();
}

class _InsertuserState extends State<Insertuser> {
  var formkey = GlobalKey<FormState>();

  var nameController = TextEditingController();
  var testController = TextEditingController();
  var imgController = TextEditingController();
  var priceController = TextEditingController();

  @override
  void initState() {
    nameController.text = widget.map == null ? '' : widget.map!["Name"];
    testController.text = widget.map == null ? '' : widget.map!["test"];
    imgController.text = widget.map == null ? '' : widget.map!["img"];
    priceController.text = widget.map == null ? '' : widget.map!["price"];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formkey,
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(hintText: 'Enter Name'),
              validator: (value) {
                if (value != null && value.isEmpty) {
                  return " Enter Valid Name";
                }
              },
              controller: nameController,
            ),
            TextFormField(
              decoration: InputDecoration(hintText: 'Enter test'),
              validator: (value) {
                if (value != null && value.isEmpty) {
                  return " Enter Valid createdAt";
                }
              },
              controller: testController,
            ),
            TextFormField(
              decoration: InputDecoration(hintText: 'Enter Price'),
              validator: (value) {
                if (value != null && value.isEmpty) {
                  return " Enter Valid Price";
                }
              },
              controller: priceController,
            ),
            TextFormField(
              decoration: InputDecoration(hintText: 'Enter ImageUrl'),
              validator: (value) {
                if (value != null && value.isEmpty) {
                  return " Enter Valid ImageUrl";
                }
              },
              controller: imgController,
            ),
            TextButton(
              onPressed: () {
                if (formkey.currentState!.validate()) {
                  if (widget.map == null) {
                    insertUser()
                        .then((value) => Navigator.of(context).pop(true));
                  } else {
                    updateUser(widget.map!['id'])
                        .then((value) => Navigator.of(context).pop(true));
                  }
                }
              },
              child: Text(
                'Submit',
                style: TextStyle(
                  backgroundColor: Colors.greenAccent,
                  fontSize: 20,

                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> updateUser(id) async {
    Map map = {};
    map['Name'] = nameController.text.toString();
    map['img'] = imgController.text.toString();
    map['test'] = testController.text.toString();
    map['price'] = priceController.text.toString();

    var response1 = await http.put(
        Uri.parse('https://63f8de8b5b0e4a127df030ce.mockapi.io/Items/$id'),
        body: map);
    print(response1.body);
  }

  Future<void> insertUser() async {
    Map map = {};
    map['Name'] = nameController.text.toString();
    map['img'] = imgController.text.toString();
    map['test'] = testController.text.toString();
    map['price'] = priceController.text.toString();

    var response1 = await http.post(
        Uri.parse('https://63f8de8b5b0e4a127df030ce.mockapi.io/Items'),
        body: map);
    print(response1.body);
  }
}
